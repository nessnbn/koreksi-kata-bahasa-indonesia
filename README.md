# Koreksi Kata Bahasa Indonesia Dengan Menggunakan Pandas, NLTK dan Sastrawi
**Project Pembelajaran Mesin Sarjana Informatika 2016**

1. Dolly Lesmana Lubis
2. Kristian Pratama Nainggolan
3. Yohannes Nababan

Melakukan tokenizing dan sebagainya untuk memperbaiki kata pada kalimat-kalimat tersebut sesuai dengan KBBI secara otomatis menggunakan Pandas, NLTK dan Sastrawi